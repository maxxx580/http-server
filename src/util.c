#include "util.h"

static pthread_mutex_t accept_lock = PTHREAD_MUTEX_INITIALIZER;

char * str_cut(const char * str, int begin, int len) {
  char * temp = malloc((len + 1)* sizeof(char));
  memset(temp, 0, len+1);
  int i;
  for (i = 0; i < len; i += 1) {
    temp[i] = str[begin+i];
  }
  temp[len] = '\0';
  return temp;
 }

 char * str_concat(const char * str1, const char * str2) {
   int len1 = strlen(str1);
   int len2 = strlen(str2);
   char * result = malloc((len1 + len2 + 1) * sizeof(char));
   memset(result, 0, len1+len2+1);
   result[len1+len2] = '\0';
   int index;

   for (index = 0; index < len1; index += 1) {
     result[index] = str1[index];
   }
   for (index = len1; index < len1 + len2; index += 1) {
     result[index] = str2[index-len1];
   }
   return result;
 }

 int find_char_in_str(const char * line, char substr, int len) {
  int i;
  for (i = 0; i < len; i += 1) {
    if (line[i] == substr) return i;
  }
  return -1;
 }

void config_parser(const char * config_file, config_info_t * info) {
  FILE * fp = fopen(config_file, "r");
  size_t len = 0;
  char * line = NULL;
  ssize_t read;
  while ((read = getline(&line, &len, fp)) != -1) {
    int index;
    if ((index = find_char_in_str(line, '=', read)) == -1) {
      printf("Invalid configuration: %s\n", line);
      continue;
    } else {
      char * name = str_cut(line, 0, index);
      char * value = str_cut(line, index+1, read-index-2);
      // printf("Name: %s, Value: %s\n", name, value);
      if (strcmp(name, "port") == 0) {
        info->port = atoi(value);
      } else if (strcmp(name, "errorlog") == 0) {
        strcpy(info->error_log_dir, value);
      } else if (strcmp(name, "accesslog") == 0) {
        strcpy(info->access_log_dir, value);
      } else if (strcmp(name, "root") == 0) {
        strcpy(info->root, value);
      } else {
        printf("name %s with value %s is not recognized\n",
                name, value);
        continue;
      }
      free(name);
      free(value);
    }
  }
  free(line);
  fclose(fp);
  return;
}

char * request_parser(const char * request) {
  int i = 0;
  char * buffer;

  if (strstr(request, "?") != NULL) {
    buffer = str_cut(request, 5, find_char_in_str(request, '?', strlen(request))-5);
    buffer = str_concat("/", buffer);
  } else if (strstr(request, "POST") != NULL) {
    buffer = str_cut(request, 6, strlen(request)-8-6-2);
    buffer = str_concat("/", buffer);
  } else {
    buffer = str_cut(request, 5, strlen(request)-8-5-2);
    buffer = str_concat("/", buffer);
  }
  return buffer;
}

char * cgi_param_parser(const char * request, const char * method) {
  // printf("trying to parse %s with method %sand compare %d\n", request, method, strcmp(method, "GET"));
  if (strcmp(method, "GET ") == 0 && strstr(request, "cgi-bin") != NULL) {
    printf("%s\n", "this is cgi get request");
    char * temp = get_n_line(request, 1);
    // temp = request_parser(temp);
    int end = strlen(temp);
    int start = find_char_in_str(temp, '?', end);
    // printf("this is get request: %s, %d, %d\n", temp, start, end);
    temp = str_cut(temp, start, end);
    return str_cut(temp, 0, find_char_in_str(temp, ' ', strlen(temp)));
  } else if (strcmp(method, "POST") == 0 && strstr(request, "cgi-bin") != NULL) {
    printf("%s\n", "this is cgi post request");

    int i = strlen(request);
    char * runner = request + i;
    while (i >= 0) {
      if (request[i] == '\n') break;
      i--;
    }
    char * temp = str_cut(request, i+1, strlen(request)-i-1);
    return str_concat("?", temp);
    // return temp;
    // int line_cnt = 0;
    // int total_len = strlen(request);
    // int index = 0;
    // while (index < total_len){
    //   if (request[index] == '\n') line_cnt++;
    //   index++;
    // }
    // return get_n_line(request, line_cnt);
  } else {
    return NULL;
  }

}

char * get_request(int fd) {
  int len = 1024;
  int nread = 0;
  char * request = (char *)malloc(1025 * sizeof(char));
  if ((nread = read(fd, request, 1024)) <= 0) {
    return "\0";
  } else {
    request[nread] = '\0';
    return request;
  }
}

char * get_n_line(const char * request, int n) {
  int i;
  int total_len = strlen(request);
  int right = 0;
  int left = 0;
  for (i = 0; i < n; i++) {
    left = right;
    while (right < total_len && request[right] != '\n') right++;
  }
  return str_cut(request, left, right-left);
}


int check_cgi(const char * filepath) {
  if(strstr(filepath, "\/cgi-bin\/") != NULL) {
    return 1;
  } else {
    return 0;
  }
}

void get_types(char * type, char * file) {
  // TODO: how to handle other fomat?
  int len = strlen(file);
  char * temp = file + len - 1;
  while (*temp != '.' && temp > file) temp--;
  if (strcmp(temp, ".htm") == 0) {
    strcpy(type, "txt/html");
  } else if (strcmp(temp, ".jpg") == 0) {
    strcpy(type, "image/jpeg");
  } else if (strcmp(temp, ".png") == 0) {
    strcpy(type, "image/png");
  } else if (strcmp(temp, ".gif") == 0) {
    strcpy(type, "image/gif");
  } else {
    strcpy(type, "txt/html");
  }
  return;
}

char * get_method(const char * line) {
  char * method;
  if ((method = strstr(line, "GET")) != NULL) {
    return str_cut(method, 0, 4);
  } else if ((method = strstr(line, "POST")) != NULL) {
    return str_cut(method, 0, 4);
  } else {
    return NULL;
  }
}

char * read_file(const char * path, int * cnt) {
  int fd;
  if ((fd = open(path, O_RDONLY)) > 0) {
    struct stat st;
    stat(path, &st);
    int size = st.st_size;
    char * temp = (char*)malloc(sizeof(char));
    char * final = (char*)malloc(size * sizeof(char));
    int count = 0;
    while (count < size) {
      read(fd, temp, 1);
      final[count] = *temp;
      count++;
    }
    *cnt = count;
    close(fd);
    free(temp);
    return final;
  } else {
    return NULL;
  }
}

int init(int port) {
  struct sockaddr_in sin;
  static int s;
  int bnd, lstn;
  bzero((char *) &sin, sizeof(sin));

  sin.sin_addr.s_addr = INADDR_ANY;
  sin.sin_family = AF_INET;
  sin.sin_port = htons((unsigned short)port);

  if ((s = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
    perror("Failed to initialize socket");
    exit(2);
  } else {}
  if ((bnd = bind(s, (struct sockaddr *)&sin, sizeof(sin))) == -1) {
    perror("Failed to bind");
    exit(2);
  } else {}
  if ((lstn = listen(s, 12)) == -1) {
    perror("Failed to listen");
    exit(2);
  } else {}
  return s;
}

int accept_connection(int socket) {
  pthread_mutex_lock(&accept_lock);
  int fd = accept(socket, NULL, NULL);
  pthread_mutex_unlock(&accept_lock);
  return fd;
}

int return_result(int fd,
                  int status,
                  char * body,
                  char * format,
                  long int fsize) {
  if (status == 200) {
    char header[128];
    sprintf(header,
            "HTTP/1.1 200 OK\n\
            Content-Type: %s\n\
            Content-Length: %ld\n\
            Connection: Close\n\n%s",
            format, fsize, "\0");

    char response[strlen(header)+ fsize];
    int i;
    int hsize = strlen(header);
    for (i = 0; i < hsize; i += 1) {
      response[i] = header[i];
    }
    for (i = hsize; i < hsize+fsize; i += 1) {
      response[i] = body[i-hsize];
    }
    write(fd, response, hsize+fsize);
    close(fd);
    return 0;
  } else if (status == 404) {
    char * response = "HTTP/1.1 404 Not Found\n\
                      Date: Sun, 18 Oct 2012 10:36:20 GMT\n\
                      Server: Apache/2.2.14 (Win32)\n\
                      Content-Length: 1024\n\
                      Connection: Closed\n\
                      Content-Type: text/html\0";

    write(fd, response, strlen(response)+1);
    close(fd);
    return 0;
  } else if (status == 500) {
    char * response = "HTTP/1.1 500 Internal Server Error\n\
                      Server: Apache-Coyote/1.1\n\
                      Content-Type: application/xml;charset=ISO-8859-1\n\
                      Content-Language: en-US\n\
                      Content-Length: 3251\n\
                      Date: Thu, 14 Oct 2010 23:22:49 GMT\n\
                      Connection: close\0";
    write(fd, response, strlen(response)+1);
    close(fd);
    return 0;
  } else {
    printf("Unknown status code to this server: %d\n", status);
    close(fd);
    return 1;
  }
}

// int check_cgi(char * path) {
//   regex_t regex;
//   int reti;
//   if (
//       (reti = regcomp(&regex, "\/cgi-bin\/", REG_EXTENDED|REG_NOSUB))
//       != 0) {
//     printf("failed to compile regex\n");
//     return -1;
//   } else {
//     reti = regexec(&regex, path, 0, NULL, 0);
//     if (!reti) {
//       return 1;
//     } else if (reti == REG_NOMATCH) {
//       return 0;
//     } else {
//       printf("regex match failed");
//       return -1;
//     }
//   }
// }
