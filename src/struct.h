#define MAX_THREADS 100
#define MAX_QUEUE_SIZE 100
#define MAX_DIR_LENGTH 1024
#define QUEUE_LENTH 100

// structs
typedef struct access_info {
  char file_name[MAX_DIR_LENGTH];
} access_info_t;

typedef struct error_info {
  char file_name[MAX_DIR_LENGTH];
  int error_code;
} error_info_t;

typedef struct log_info {
  int status;
  char filename[1024];
} log_info_t;

typedef struct config_info {
  int port;
  int t_num;
  char * root;
  char * error_log_dir;
  char * access_log_dir;
} config_info_t;

typedef struct request_info {
  int socket;
  char * filename;
  int isCGI;
  char * method;
  char * params;
} request_info_t;
