#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <netinet/in.h>
#include <pthread.h>
#include <regex.h>
#include "struct.h"

enum boolean {FALSE, TRUE};

/*************** connection operation ****************/
int init(int port);
int accept_connection(int socekt);
char * get_request(int fd);
int return_result(int fd,
                  int error,
                  char * body,
                  char * format,
                  long int fsize);
/*************** ****parser **************************/
char * request_parser(const char * request);
void config_parser(const char * config_file,
                  config_info_t * info);
char * cgi_param_parser(const char * request, const char * method);

/*************** string operation *******************/
char * str_cut(const char *str, int begin, int len);
char * str_concat(const char * str1, const char * str2);
int find_char_in_str(const char * line, char substr, int len);
char * get_n_line(const char * str, int nline);


/************* other helper function ****************/
int check_cgi(const char * filepath);
void get_types(char * type, char * file);
char * read_file(const char * path, int * cnt);
char * get_method(const char * line);
