#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <sys/wait.h>
#include "util.h"
// locks
static pthread_mutex_t log_write = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t buffer_access = PTHREAD_MUTEX_INITIALIZER;

static pthread_cond_t buffer_empty = PTHREAD_COND_INITIALIZER;
static pthread_cond_t buffer_full = PTHREAD_COND_INITIALIZER;

// request queue
request_info_t queue[QUEUE_LENTH];
static int front = 0, rear = 0;

static int server_socket;
char root[1024];
config_info_t info;

/********* printing config for testing purporses ********/
void test_config() {
  printf("port: %d\n", info.port);
  printf("root: %s\n", info.root);
  printf("error log: %s\n", info.error_log_dir);
  printf("access log: %s\n", info.access_log_dir);
}

/********* add a request from request queue ********/
void queue_add(request_info_t temp) {
  queue[(front++)%QUEUE_LENTH] = temp;
  return;
}
/********* remove a request from request queue *****/
request_info_t queue_get() {
  request_info_t temp;
  temp = queue[(rear++)%QUEUE_LENTH];
  return temp;
}

/********* release port ***************************/
void signal_handler(int sig) {
  printf("in sig hand\n");
  pid_t pid = getpid();
  close(server_socket);
  kill(pid, 9);
}

/********* log error or access activities ********/
void logger(log_info_t logs) {
 pthread_mutex_lock(&log_write);
 FILE * fp = NULL;
 if (logs.status == 200) {  // success and write in access log
  fp = fopen(info.access_log_dir, "a");
  fprintf(fp, "[%s][%d]\n", logs.filename, logs.status);
  fclose(fp);
 } else {  // error and write in error log
  fp = fopen(info.error_log_dir, "a");
  fprintf(fp, "[%s][%d]\n", logs.filename, logs.status);
  fclose(fp);
 }
 pthread_mutex_unlock(&log_write);
 return;
}

/************** accept request and add to queue ***************/
void dispatch() {
  while (1) {
    request_info_t temp;
    temp.socket = -1;
    // connect socket
    if ((temp.socket = accept_connection(server_socket)) != -1) {
      printf("Dispatch: server connected %d\n", temp.socket);
      // get request
      char * raw_request = get_request(temp.socket);
      // printf("test post raw request%s\n", raw_request);
      if (strlen(raw_request) == 0) continue;
      char * first_line = get_n_line(raw_request, 1);
      temp.isCGI = check_cgi(first_line);
      temp.filename = request_parser(first_line);
      temp.method = get_method(first_line);
      temp.params = cgi_param_parser(raw_request, temp.method);
      // printf("CGI: %d, filename: %s, method: %s, params: %s\n",
      //         temp.isCGI, temp.filename, temp.method, temp.params);
      // if (strlen(temp.filename) > 0) {
        // printf("request received:%s\n", temp.filename);
        pthread_mutex_lock(&buffer_access);
        while ((front - rear) == QUEUE_LENTH)
          pthread_cond_wait(&buffer_empty, &buffer_access);
        queue_add(temp);
        pthread_cond_signal(&buffer_full);
        pthread_mutex_unlock(&buffer_access);
    } else {
      printf("Server failed to connected\n");
      continue;
    }
  }
  pthread_exit(NULL);
}

/********* get a request from queue and process requests ********/
void worker() {
  while (1) {
    pthread_mutex_lock(&buffer_access);
    while ((front - rear) == 0)
      pthread_cond_wait(&buffer_full, &buffer_access);
    request_info_t temp;
    log_info_t log_info;

    temp = queue_get();
    pthread_cond_signal(&buffer_empty);
    pthread_mutex_unlock(&buffer_access);
    if (temp.isCGI == 0) {   // non cgi script request
      FILE * fp = NULL;
      char * filepath = str_concat(info.root, temp.filename);
      char * type = (char*) malloc(1024 * sizeof(char));
      char * buff_in;
      if ((fp = fopen(filepath, "r")) == NULL) {
        char * msg = strerror(errno);
        printf("errno message: %s %s\n", msg, filepath);
        return_result(temp.socket, 404, buff_in, type, 0);
        strcpy(log_info.filename, filepath);
        log_info.status = 404;
      } else {
        fseek(fp, 0, SEEK_END);
        long fsize = ftell(fp);
        char buff_in[fsize+1];
        rewind(fp);
        fread(buff_in, fsize, 1, fp);
        get_types(type, filepath);
        return_result(temp.socket, 200, buff_in, type, fsize);
        strcpy(log_info.filename, filepath);
        log_info.status = 200;
        free(type);
        free(filepath);
        fclose(fp);
      }
    } else if (temp.isCGI == 1) {   // cgi script request
      char * argv[3];
      argv[0] = "python";
      argv[1] = str_concat(info.root, temp.filename);
      argv[2] = NULL;
      // printf("test parameters: %s and test script path %s\n", temp.params, argv[1]);
      if (setenv("QUERY_STRING", temp.params, 1) == -1) {
        perror("Failed to set env variable");
        return_result(temp.socket, 500, NULL, NULL, 0);
        strcpy(log_info.filename, argv[1]);
        log_info.status = 500;
      }
      printf("Forking process\n");
      int pid = fork();
      if (pid > 0) {   // parent process
        int status = 0;
        waitpid(pid, &status, 0);
        strcpy(log_info.filename, argv[1]);
        log_info.status = 202;
      } else if (pid == 0) {   // child process
        close(1);
        dup2(temp.socket, 1);
        execvp(argv[0], argv);
        exit(0);
      }
    } else {
      return_result(temp.socket, 500, NULL, NULL, 0);
      strcpy(log_info.filename, temp.filename);
      log_info.status = 500;
      printf("unknown request type\n");
    }
    logger(log_info);
  }
  pthread_exit(NULL);
}

int main(int argc, char **argv) {
  if (argc != 2 && argc != 1) {
    printf("One or two arguements are needed\n");
    exit(1);
  }
  char config_path[1024] = "/home/maxxx580/Desktop/WebServer/conf/httpd.conf";
  if (argc == 2) {
    // config_path = argv[1];
    strcpy(config_path, argv[1]);
  }

  printf("config file: %s\n", config_path);

  info.root = (char*)malloc(1024 * sizeof(char));
  info.error_log_dir = (char*)malloc(1024 * sizeof(char));
  info.access_log_dir = (char*)malloc(1024 * sizeof(char));
  config_parser(config_path, &info);

  chdir(info.root);
  char cwd[1024];
  if (getcwd(cwd, sizeof(cwd)) == NULL) {
    perror("Failed to get cwd");
  } else {
    strcpy(root, cwd);
  }
  test_config();
  printf("Server current directory: %s\n", root);

  server_socket = init(info.port);

  // add signal hadler so it will clean up port
  struct sigaction act;
  act.sa_handler = signal_handler;
  act.sa_flags = 0;
  sigemptyset(&act.sa_mask);
  sigaction(SIGINT, &act, NULL);
  sigaction(SIGKILL, &act, NULL);
  sigaction(SIGSEGV, &act, NULL);

  int cnt;
  pthread_t workers[MAX_THREADS];
  pthread_t dispatchers[MAX_THREADS];
  for (cnt = 0; cnt < MAX_THREADS; cnt += 1) {
    pthread_create(&workers[cnt], NULL, (void*)worker, NULL);
  }
  for (cnt = 0; cnt < MAX_THREADS; cnt += 1) {
    pthread_create(&dispatchers[cnt], NULL, (void*)dispatch, NULL);
  }
  for (cnt  = 0; cnt < MAX_THREADS; cnt += 1) {
    if (pthread_join(dispatchers[cnt],NULL) != 0) {
      fprintf(stderr,"Error joining consumer thread\n");
    } else {
      fprintf(stderr,"Joined consumer thread\n");
    }
  }
  for (cnt  = 0; cnt < MAX_THREADS; cnt += 1) {
    if (pthread_join(workers[cnt], NULL) != 0) {
      fprintf(stderr,"Error joining consumer thread\n");
    } else {
      fprintf(stderr,"Joined consumer thread\n");
    }
  }
  return 0;
}


// int main (int argc, char **argv) {
// //   // char * temp; // = (char *) malloc(1024 * sizeof(char));
// //   //int fd = open("test.txt", O_RDONLY);
// //   //get_request(fd, temp);
// //   //printf("testing : %s\n", temp);
// //   // char * a = "abfded\ndsdfsdfs";
// //   // temp = request_parser(input);
// //   // printf("in main: %s\n", temp);
// //
// //   // char * b = "as\ndfsdfsdfsadfasfd";
// //   // char * c = str_concat(a, b);
// //   // char * temp = "adfsfjs/cgi/sdfadf";
// //   // int c = check_cgi(temp);
// //   // printf("test: %d\n", c);
// //
// //   // info.root = (char*)malloc(1024 * sizeof(char));
// //   // info.error_log_dir = (char*)malloc(1024 * sizeof(char));
// //   // info.access_log_dir = (char*)malloc(1024 * sizeof(char));
// //   // config_parser(argv[1], &info);
// //   // test_config();
// //   // char * temp = (char*)malloc(1024 * sizeof(char));
// //   // get_types(temp, "sdfds.jpg.htm");
// //   // printf("test: %s\n", temp);
// //
// //   // int i = 0;
// //   // char * temp = read_file("/home/maxxx580/Desktop/WebServer/resource/images/05_10.gif", &i);
// //   // printf("test: %s\n", temp);
// //
// //   // char * temp = cgi_param_parser("adfsfsfs?1234567\0");
// //   // printf("test: %s\n", temp);
// //   printf("set: %d\n", setenv("QUERY_STRING", "abcdefg", 1));
// //   printf("get: %s\n", getenv("QUERY_STRING"));
// //   int pid = fork();
// //   if (pid > 0) {
// //     int status = 0;
// //     waitpid(pid, &status, 0);
// //   } else if (pid == 0) {
// //     printf("%s\n", "this is child");
// //     char * args[3];
// //     args[0] = "python";
// //     args[1] = "/home/maxxx580/Desktop/WebServer/cgi-bin/script1.py";
// //     args[2] = NULL;
// //     execvp(args[0], args);
// //   }
// //   // while(1);
//
// char temp[1024] = "GET /resource/cgi-bin/index.htm?a=1&b=7 HTTP/1.0\n\
//                   Host: [rsid].112.2o7.net\n\
//                   X-Forwarded-For: 192.168.10.1\0";
// // char * out0 = get_request()
// char * out1 = get_n_line(temp, 1);
// char * out2 = request_parser(out1);
// char * out4 = cgi_param_parser(temp, "GET");
// char * out3 = get_method(out1);
//
// printf("out 1: %s\n", out1);
// printf("out 2: %s\n", out2);
// printf("out 3: %s\n", out3);
// printf("out 4: %s\n", out4);
//
//   return 0;
// }
