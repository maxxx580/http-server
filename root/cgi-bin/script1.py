#!/usr/bin/python
# Import modules for CGI handling
import cgi, cgitb
import os

# This cgi script add two number and return the sum


# Create instance of FieldStorage
# form = cgi.FieldStorage()

# Get data from fields
# first_name = form.getvalue('first_name')
# last_name  = form.getvalue('last_name')

# print "I am in python"
request = os.environ.get('QUERY_STRING')

def get_params(line):
    params = [];
    left = 1
    right  = 1
    str_length = len(line)
    while (right < str_length):
        while (line[left] != '='):
            left += 1
            right += 1
        left += 1
        right = left
        while (right < str_length and line[right] != '&'):
            right += 1
        temp = line[left:right]
        params.append(temp)
        left = right
    return params


params = get_params(request);

response = "<html>\n<head>\n<title>Hello - sum two numbers</title>\n</head>\n<body>\n<p>This is a testing cgi script for INET4021 Project 1.<br> The sum of %d and %d is %d</p>\n</body>\n</html>" % (int(params[1]), int(params[0]), int(params[1])+int(params[0]))

print "HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: %d\nConnection: Close\n\n%s" % (len(response), response);


# print "<html>"
# print "<head>"
# print "<title>Hello - Second CGI Program</title>"
# print "</head>"
# print "<body>"
# print "<p>This is the sum of %d and %d: %d</p>" % (params[1], params[0], params[1]+params[0])
#
# # print "<h2>Hello %s %s</h2>" % (first_name, last_name)
# print "</body>"
# print "</html>"
