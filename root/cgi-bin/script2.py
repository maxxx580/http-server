#!/usr/bin/python
# Import modules for CGI handling
import cgi, cgitb
import os

# This cgi script add two number and return the sum

# Create instance of FieldStorage
# form = cgi.FieldStorage()

# Get data from fields
# first_name = form.getvalue('first_name')
# last_name  = form.getvalue('last_name')

# print "I am in python"
request = os.environ.get('QUERY_STRING')

def get_params(line):
    params = [];
    left = 1
    right  = 1
    str_length = len(line)
    while (right < str_length):
        while (line[left] != '='):
            left += 1
            right += 1
        left += 1
        right = left
        while (right < str_length and line[right] != '&'):
            right += 1
        temp = line[left:right]
        params.append(temp)
        left = right
    return params
params = get_params(request);

response = "<html>\n<head>\n<title>Message received</title>\n</head>\n<body>\n<p>This is a testing cgi script for INET4021 Project 1.<br> Message received are %s and %s</p>\n</body>\n</html>" % (params[0], params[1])

print "HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: %d\nConnection: Close\n\n%s" % (len(response), response);
