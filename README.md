# HTTP Server
## Description
This is an multi-thread HTTP server that is capable of handling GET and POST request from a client. The program consist the following structures. The src/ folder hosts all source code of this program. The bin/ folder contains the server executable and bin/objects/ contains all object file. The root/ directory is the root of the server and it contains a sample website used for testing purposes. The cgi-bin/ residing under root directory directory includes all common gate interface scripts.
```
WebServer/
    conf/
      httpd.conf
    logs/
      access.log
      error.log
    bin/
      objects/
    src/
      server.c
      struct.h
      util.c
      util.h
    root/
      cgi-bin/
        srcipt1.py
        script2.py
        script3.py
      *.htm
      *.gif
      images/
    makefile
    README.md
```
The server is meant to handle HTTP GET and POST methods only. There are three sample cgi-scripts available in WebServer/cgi-bin/ directory and by request the scripts, they will be executed by the server program.

## How to compile and use it   
A makefile is created to compile this program. Direct to /WebSever/ directory and use: `$ make`. The make will compile the program and creates the executable file under folder bin/. `$ make clean` will remove the server executable and objective files from bin folder.

Execute `$ ./bin/server` command will starts the server with default configuration specified in WebServer/conf/httpd.conf. Using `$ ./bin/server {path to config file}` will load specified configuration file and starts server with these setting.

## CGI
There are three cgi scripts,  written in Python and included for testing purposes, and additional scripts can be added to root/cig-bin/ directory. The script1.py takes in two integer parameters and return the sum of them. The script2.py takes in two parameters in any format and directly return to users. The scripts3.py takes in arbitrary number of parameters in string format. It will sort them into lexicographical order and return to users.    

## How to test the program
A web browser, such as Chrome and FireFox, can be used to send GET request to the sever.The following example send an get request to sever on port 9000 to ask for index.htm.  

`localhost:9000/index.htm`   

When accessing cgi scripts, parameters can be added following the path of the scripts. The following example shows a cgi request on script1.py with parameter 100 and 200. The parameter starts with character '?' and every parameter is separated by character '&' with another parameter.   

`localhost:9000/cgi-bin/script1.py?param1=100&parame2=200`

The POST request can be tested with the root/post.htm page showed below. This example is pointing to /cgi-bin/script2.py on port 9000. Parameters are login and password.

```
<HTML>
  <H1> Hello world</H1>
  <FORM ACTION=http://localhost:9000/cgi-bin/script2.py METHOD=POST>
    login: <INPUT NAME=LOGIN>
    password: <INPUT NAME=PASSWORD>
    <INPUT TYPE=SUBMIT NAME=GO>
  </FORM>
  <H2> Thank you</H2>
</HTML>

```  


## Assumption and limitation
* This program is developed under Linux and tested with Chrome browser.
* Server only handle GET and POST method.   
* Server will only return status 200, 404, and 500.   
* Non-fatal error will be ignored by the server.   
* Successful response will be logged in access log. Any failed response will be logged in error log with error code.
* Server is able to catch SIGSEGV, SIGINT, and SIGKILL and free port before the program exit.
* CGI scripts are expecting correct number of parameters. Additional parameters will be ignored. Insufficient number of parameter or incorrect parameter format will results in script error.


## Contributor
Zixiang Ma
