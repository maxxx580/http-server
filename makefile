CC = gcc
CFLAGS = -D_REENTRANT
LDFLAGS = -lpthread

all: server

server: src/server.c bin/objects/util.o
	gcc -g -o bin/server src/server.c bin/objects/util.o -lpthread

bin/objects/util.o: src/util.h src/util.c
	gcc -g -o bin/objects/util.o -c src/util.c

clean:
	rm -r bin/server bin/objects/*.o

	# /home/maxxx580/Desktop/WebServer
